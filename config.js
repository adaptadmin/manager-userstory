// var Cacheman = require('cacheman');
//
// var storage_options = {
//   engine: 'redis',
//   port: process.env.REDIS_SERVER_PORT || 6379,
//   host: process.env.REDIS_SERVER_HOST || "127.0.0.1",
//   password: process.env.REDIS_SERVER_PASSWORD || undefined,
//   prefix: process.env.REDIS_PREFIX || "handsome",
//   ttl: -1
// };
//
// exports.getStorage = function() {
//   return new Cacheman('handsome', storage_options)
// };
var Cacheman = require('cacheman');



// var storage_options = {

//   engine: 'redis',

//   port: process.env.REDIS_SERVER_PORT || 6379,

//   host: process.env.REDIS_SERVER_HOST || "127.0.0.1",

//   password: process.env.REDIS_SERVER_PASSWORD || undefined,

//   prefix: process.env.REDIS_PREFIX || "handsome",

//   ttl: -1

// };



const masterMongoDBName = process.env.APP_DB || 'CDAP';



const mongo = {

  host: process.env.MONGO_HOST || '127.0.0.1',

  port: process.env.MONGO_PORT || 27017

};



const mongoURL = ('mongodb://' + mongo.host + ':' + mongo.port + '/' +

  masterMongoDBName);



var storage_options = {

  WWW_PORT: process.env.Max_WWW_PORT || process.env.PORT || 8080,

  MONGO_MASTER_DB_NAME: masterMongoDBName,

  MONGO_MASTER_SERVER: mongo,

  MONGO_URL: mongoURL

  // NEO4J_HOST: 'localhost',
  //
  // NEO4J_BOLT_URL: ('bolt://localhost'),
  //
  // NEO4J_USR: 'neo4j',
  //
  // NEO4J_PWD: 'neoisgraph'

}



exports.getStorage = function() {

  return new Cacheman('handsome', storage_options)

};
