/* eslint-disable no-console, no-unused-vars */
var express = require('express');
var exphbs  = require('express-handlebars');
var app = express();
var moment = require('moment');
const multer = require('multer');

// var config = require(__dirname + '/config.js');
// var storage = config.getStorage();
const path = require('path');

var fs = require('fs');
var CsvReadableStream = require('csv-reader');
// var inputStream = fs.createReadStream('./sample.csv', 'utf8');
// inputStream
//     .on('data', function (row) {
//         console.log('A row arrived: ', row);
//     })
//     .on('end', function (data) {
//         console.log('No more rows!');
//     });

app.engine('hbs', exphbs({defaultLayout: 'index', extname: '.hbs', layoutsDir: 'views/'}));
app.set('view engine', 'hbs');

app.set('port', (process.env.PORT || 3002));

app.get('/csv',function(req,res) {
  var CSV_Data = [];
  var inputStream = fs.createReadStream('/var/lib/jenkins/workspace/adaptlite/sample.csv', 'utf8');
  inputStream
      .on('data', function (row) {
        // console.log(row);
        let csvlines = [];
          csvlines = row.split('\n');
          // Return array of string values, or NULL if CSV string not well formed.
          // console.log(csvlines);
            function CSVtoArray(text) {
                var re_valid = /^\s*(?:'[^'\\]*(?:\\[\S\s][^'\\]*)*'|"[^"\\]*(?:\\[\S\s][^"\\]*)*"|[^,'"\s\\]*(?:\s+[^,'"\s\\]+)*)\s*(?:,\s*(?:'[^'\\]*(?:\\[\S\s][^'\\]*)*'|"[^"\\]*(?:\\[\S\s][^"\\]*)*"|[^,'"\s\\]*(?:\s+[^,'"\s\\]+)*)\s*)*$/;
                var re_value = /(?!\s*$)\s*(?:'([^'\\]*(?:\\[\S\s][^'\\]*)*)'|"([^"\\]*(?:\\[\S\s][^"\\]*)*)"|([^,'"\s\\]*(?:\s+[^,'"\s\\]+)*))\s*(?:,|$)/g;
                // Return NULL if input string is not well formed CSV string.
                if (!re_valid.test(text)) return null;
                var a = [];                     // Initialize array to receive values.
                text.replace(re_value, // "Walk" the string using replace with callback.
                    function(m0, m1, m2, m3) {
                        // Remove backslash from \' in single quoted values.
                        if      (m1 !== undefined) a.push(m1.replace(/\\'/g, "'"));
                        // Remove backslash from \" in double quoted values.
                        else if (m2 !== undefined) a.push(m2.replace(/\\"/g, '"'));
                        else if (m3 !== undefined) a.push(m3);
                        return ''; // Return empty string.
                    });
                // Handle special case of empty last value.
                if (/,\s*$/.test(text)) a.push('');
                return a;
            };
          for(let i=1;i<csvlines.length-1;i++) {
            let csv_separator = CSVtoArray(csvlines[i]);
            // let csvwords = [];
            // csvwords = csvlines[i].split(',');
             // console.log(csv_separator,typeof csv_separator);
              var obj={
                title :csv_separator[0],
                data : csv_separator[1],
                link : csv_separator[2]
              };
              CSV_Data.push(obj);
          }
      })
      .on('end', function (data) {
          // console.log('No more rows!');
          // console.log(CSV_Data);
        res.send(CSV_Data)
      });
})

let storage1 = multer.diskStorage({
  destination: function(req, file, cb) {

      cb(null, './');
  },filename: function(req, file, cb) {
    var name = 'sample.csv';
    cb(null, name);
  }
});

// User bulk upload multer
const upload = multer({storage: storage1});

app.post('/upload', upload.any('csv'),  function(req,res) {
  });




app.get('/', function (req, res) {
  res.render('index', {name: 'index'});
});

app.get('/:dashboard', function(req, res) {
  res.render('index', {name: req.params.dashboard, layout: false});
});
app.listen(app.get('port'), function () {
  console.log("Up and running on port " + app.get('port'));
});

// Serve our bundle
if(process.env.NODE_ENV === 'production') {
  // serve the contents of the build folder
  app.use("/assets", express.static('build'));
} else {
  // serve using webpack middleware
  var webpack = require('webpack');
  var webpackDevMiddleware = require('webpack-dev-middleware');
  var webpackConfig = require('./webpack.config');
  var compiler = webpack(webpackConfig);
  app.use(webpackDevMiddleware(compiler, {
     publicPath: '/assets/',
     stats: {colors: true}
  }));
}

// load our jobs
require('./jobs.js');
