import React from 'react';
import ReactDOM from 'react-dom';
import Packery from 'packery';
import ImageWidget from 'widgets/image_widget';
import ListWidget from 'widgets/list_widget';
import NumberWidget from 'widgets/number_widget';
import FirstWidget from 'widgets/first_widget';
import SecondWidget from 'widgets/second_widget';
import ThirdWidget from 'widgets/third_widget';
import FourthWidget from 'widgets/fourth_widget';
import FifthWidget from 'widgets/fifth_widget';
import SixthWidget from 'widgets/sixth_widget';
import SeventhWidget from 'widgets/seventh_widget';
import GaugeWidget from 'widgets/gauge_widget';
import EighthWidget from 'widgets/eighth_widget';
import NinthWidget from 'widgets/ninth_widget';
import Tenth_widget from 'widgets/tenth_widget';
import Eleventh_widget from 'widgets/eleventh_widget';
import TextWidget from 'widgets/text_widget';

import $ from 'jquery';

export default class App extends React.Component {
  constructor() {
    super();
    this.state = {
      First_widget : "",
      Second_widget : "",
      Third_widget : "",
      Fourth_widget : "",
      Fifth_widget : "",
      Sixth_widget : '',
      Seventh_widget: '',
      Eighth_widget : '',
      Ninth_widget : '',
      Tenth_widget : '',
      Eleventh_widget : '',
      Gauge_widget : ''

    }
  }

componentWillMount() {
  let context = this;
  $.ajax({
    url : '/csv',
    type : 'GET',
    success : function(response) {
      // console.log(response);
      context.setState({ First_widget : response[0],Second_widget : response[1],Third_widget : response[2],Fourth_widget : response[3],
                        Fifth_widget : response[4],Sixth_widget : response[5],Seventh_widget : response[7],Eighth_widget : response[8],
                      Ninth_widget : response[9],Tenth_widget : response[10],Eleventh_widget : response[11],Gauge_widget : response[6]})
    }, error : function(err) {
      console.log('error',err);
    }
  })
}
  render() {
    return (
      <div id="dashboard">
        <a href = {this.state.First_widget.link} target='_blank'><FirstWidget  First_widget = {this.state.First_widget} width='2'/></a>
        <a href = {this.state.Second_widget.link} target='_blank'><SecondWidget  Second_widget = {this.state.Second_widget}  width='1'/></a>
        <a href = {this.state.Third_widget.link} target='_blank'><ThirdWidget Third_widget = {this.state.Third_widget}  width='1.5'/></a>
        <a href = {this.state.Fourth_widget.link} target='_blank'><FourthWidget Fourth_widget = {this.state.Fourth_widget}  width='1'/></a>
        <a href = {this.state.Fifth_widget.link} target='_blank'><FifthWidget Fifth_widget = {this.state.Fifth_widget}  width='2'/></a>
        <a href = {this.state.Sixth_widget.link} target='_blank'><SixthWidget Sixth_widget = {this.state.Sixth_widget}  width='1'/></a>
        <a href = {this.state.Gauge_widget.link} target='_blank'><GaugeWidget Gauge_widget = {this.state.Gauge_widget}  width='2'/></a>
        <a href = {this.state.Seventh_widget.link} target='_blank'><SeventhWidget Seventh_widget = {this.state.Seventh_widget}  width='2'/></a>
        <a href = {this.state.Eighth_widget.link} target='_blank'><EighthWidget  Eighth_widget = {this.state.Eighth_widget}  width='1'/></a>
        <a href = {this.state.Ninth_widget.link} target='_blank'><NinthWidget Ninth_widget = {this.state.Ninth_widget}  width='2'/></a>
        <a href = {this.state.Tenth_widget.link} target='_blank'><Tenth_widget Tenth_widget = {this.state.Tenth_widget}  width='2'/></a>
        <a href = {this.state.Eleventh_widget.link} target='_blank'><Eleventh_widget Eleventh_widget = {this.state.Eleventh_widget}  width='2'/></a>
      </div>
    );
  }
}
