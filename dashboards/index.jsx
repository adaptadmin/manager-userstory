import React from 'react';
import ReactDOM from 'react-dom';
import Packery from 'packery';
import ImageWidget from 'widgets/image_widget';
import ListWidget from 'widgets/list_widget';
import NumberWidget from 'widgets/number_widget';
import TextWidget from 'widgets/text_widget';
import App from './app.jsx';

import "styles/default.scss";


ReactDOM.render(
  <App/>,
  document.getElementById('content')
);

new Packery("#dashboard", {itemSelector: ".widget", gutter: 12});
