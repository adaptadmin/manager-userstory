import React from 'react';
import ImageWidget from 'widgets/image_widget';
import BaseWidget from './widget.jsx';
import Numeral from 'numeral';
import { Image,Feed,Button } from 'semantic-ui-react'
import './first_widget.scss';
import {Form, Input, Grid, Icon} from 'semantic-ui-react';
import FileUploadProgress  from './fileUpload.jsx';
import {CSVLink, CSVDownload} from 'react-csv';

export default class FirstWidget extends BaseWidget {

  constructor(props) {
    super(props);
    this.state = {title: "init",
    number: 0,
    checkfile: true
  };
     this.checkfile = this.checkfile.bind(this);
  }
  checkfile(sender) {
    //console.log(sender.target.value);
     var validExts = new Array(".csv");
     var fileExt = sender.target.value;
     fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
     if (validExts.indexOf(fileExt) < 0) {
       alert("Invalid file selected, valid files are of " +
                validExts.toString() + " types.");
                sender.target.value = "";
                this.setState({checkfile:true});
       return false;
     }
     else {
       this.setState({checkfile:false});
       return true;
     }
 }
  render() {
    const csvData = [['Title','value','link'],
                ['23-04-2018','http://','http://maxcdn.webappers.com/img/2012/11/dashboard-framework.png'],
                ['Release Number','10','http://maxcdn.webappers.com/img/2012/11/dashboard-framework.png'],
                ['Sprint Number','5','http://maxcdn.webappers.com/img/2012/11/dashboard-framework.png'],
                ['Team Members','20','http://maxcdn.webappers.com/img/2012/11/dashboard-framework.png'],
                ['Burn Up Chart','{sprint : [1,2,3,4,5,6,7,8,9,10],dataset1 : [80,80,80,80,75,75,75,75,75,75],dataset2 : [0,5,11,19,24]}','http://maxcdn.webappers.com/img/2012/11/dashboard-framework.png'],
                ['Post Sprint Defects','9','http://maxcdn.webappers.com/img/2012/11/dashboard-framework.png'],
                ['Test Automation Coverage','85%','http://maxcdn.webappers.com/img/2012/11/dashboard-framework.png'],
                ['Velocity Trend','{sprint : [1,2,3,4,5,6],dataset : [50,55,80,70]}','http://maxcdn.webappers.com/img/2012/11/dashboard-framework.png'],
                ['PCSAT','6','http://maxcdn.webappers.com/img/2012/11/dashboard-framework.png'],
                ['Sprint Burn Down','{sprint:[1,2,3,4,5,6,7],dataset:[15,15,14,13,11,18]}','http://maxcdn.webappers.com/img/2012/11/dashboard-framework.png'],
                ['User Stories Commited vs Delivered','{sprint:[1,2,3,4,5,6],dataset1 : [10,8,12,8],dataset2 : [8,8,10,8,2]}','http://maxcdn.webappers.com/img/2012/11/dashboard-framework.png'],
                ['Requirement Volatility','{sprint:[1,2,3,4,5,6],dataset : [10,0,25,15,0,20]}','http://maxcdn.webappers.com/img/2012/11/dashboard-framework.png']
              ];
    return (
      <div className={"number_widget widget w" + this.props.width + " h" + this.props.height}>
        <Image src='https://upload.wikimedia.org/wikipedia/en/thumb/3/3d/Wipro_logo.svg/1280px-Wipro_logo.svg.png' size='small'  />
        <Feed>
         <Feed.Event>
           <Feed.Content>
               <Feed.Date>{this.props.First_widget.title}</Feed.Date>
           </Feed.Content>
         </Feed.Event>
        </Feed>
        <div style={{width:'220%',marginTop:"-1%",color:'black'}}>
           <p style={{fontFamily:'sans-serif'}}>To download sample template <CSVLink data={csvData} filename={"UserTemplate.csv"}><br/>Click here</CSVLink></p>
          <div style={{width:'73%',float:'right',marginTop:'-6%'}}>
               <FileUploadProgress key='ex1' url='/upload' accept='.csv'
                  onProgress={(e, request, progress) => {
                    //console.log('progress', e, request, progress);
                  }}
                  onLoad={ (e, request) => {
                    //console.log('load', e, request);
                  }}
                  onError={ (e, request) => {
                    //console.log('error', e, request);
                  }}
                  onAbort={ (e, request) => {
                    //console.log('abort', e, request);
                  }}
                  onClick = {this.checkfile}
                  />
          </div>
           </div>
        {/* <Button floated='right' content='upload csv' icon='upload' labelPosition='left' /><br/> */}
        <p style={{marginTop:'5%',color:'#767676',fontFamily:'sans-serif'}}>powered by  Digital Rig.</p>
      </div>
    );
  }
}

FirstWidget.defaultProps.formatString = '0.[00]a';
