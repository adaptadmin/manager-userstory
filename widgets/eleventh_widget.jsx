import React from 'react';
import BaseWidget from './widget.jsx';
import {Bar} from 'react-chartjs-2';
import './eleventh_widget.scss';

export default class EleventhWidget extends BaseWidget {

  constructor(props) {
    super(props);
    this.state = {title: "init", list: ["init"]};
  }

  render() {
    let context = this;
    let Json_data = "";

    if(this.props.Eleventh_widget.data != undefined){
      let data = context.props.Eleventh_widget.data
      data = data.replace(/ /g, '');
      data = data.replace('sprint','"sprint"')
      data = data.replace('dataset','"dataset"')
      Json_data = JSON.parse(data)
      // console.log(Json_data,".................");
    }
    const data = {
  labels: Json_data.sprint,
  options: {
      maintainAspectRatio: false,
      legend: {
          labels: {
              fontColor: "black",
              fontSize: 12,
              fontFamily:'sans-serif'
          }
      },
    scales: {
        xAxes: [{
            gridLines: {
            },
            ticks: {
              fontColor: "black", // this here
            },
            scaleLabel: {
              display: true,
              labelString: 'number of sprints',
              fontColor: "black",
              fontSize: 12,
              fontFamily:'sans-serif'
            }
        }],
        yAxes: [{
            display: true,
            gridLines: {

            },
            ticks: {
              fontColor: "black", // this here
              fontFamily:'sans-serif'
            },
            scaleLabel: {
              display: true,
              labelString: 'volatility',
              fontColor: "black",
              fontSize: 12,
              fontFamily:'sans-serif'
            }
        }],
    }
},
  datasets: [
    {
      label: "Requirement Volatility",
      backgroundColor: 'blue',
      borderColor: 'blue',
      borderWidth: 1,
      hoverBackgroundColor: 'blue',
      hoverBorderColor: 'blue',
      data: Json_data.dataset
    },
  ]
};
    return (
      <div  className={"eleventh_widget widget w" + this.props.width + " h" + this.props.height}>
        <h2>{this.props.Eleventh_widget.title}</h2>
        <div>
          <Bar
              data={data}
              width={200}
              height={180}
              options={data.options}
            />
      </div>
      </div>
    );
  }
}
