import React from 'react';
import BaseWidget from './widget.jsx';
import {Bar} from 'react-chartjs-2';
import './tenth_widget.scss';

export default class TenthWidget extends BaseWidget {

  constructor(props) {
    super(props);
    this.state = {title: "init", list: ["init"]};
  }

  render() {
    let context = this;
    let Json_data = "";
    if(this.props.Tenth_widget.data != undefined){
      let data = context.props.Tenth_widget.data
      data = data.replace(/ /g, '');
      data = data.replace('sprint','"sprint"')
      data = data.replace('dataset1','"dataset1"')
      data = data.replace('dataset2','"dataset2"')
      Json_data = JSON.parse(data)
      // console.log(Json_data,".................");
    }
    const data = {
  labels: Json_data.sprint,
  options: {
    maintainAspectRatio: false,
    legend: {
        labels: {
            fontColor: "black",
            fontSize: 12,
            fontFamily:'sans-serif'
        }
    },
    scales: {
        xAxes: [{
            gridLines: {
            },
            ticks: {
              fontColor: "black", // this here
            },
            scaleLabel: {
              display: true,
              labelString: 'number of sprints',
              fontColor: "black",
              fontSize: 12,
              fontFamily:'sans-serif'
            }
        }],
        yAxes: [{
            display: true,
            gridLines: {

            },
            ticks: {
              fontColor: "black", // this here
              fontFamily:'sans-serif'
            },
            scaleLabel: {
              display: true,
              labelString: 'story points',
              fontColor: "black",
              fontSize: 12,
              fontFamily:'sans-serif'
            }
        }],
    }
},
  datasets: [
    {
      label: "Commited Stories",
      backgroundColor: 'orange',
      borderColor: 'orange',
      borderWidth: 1,
      hoverBackgroundColor: 'orange',
      hoverBorderColor: 'orange',
      data: Json_data.dataset1
    },
    {
      label: "Delivered Stories",
      backgroundColor: 'green',
      borderColor: 'green',
      borderWidth: 1,
      hoverBackgroundColor: 'green',
      hoverBorderColor: 'green',
      data: Json_data.dataset2
    }
  ]
};
    return (
      <div  className={"tenth_widget widget w" + this.props.width + " h" + this.props.height}>
        <h2>{this.props.Tenth_widget.title}</h2>
        <div>
          <Bar
              data={data}
              width={200}
              height={180}
              options={data.options}
            />
      </div>
      </div>
    );
  }
}
