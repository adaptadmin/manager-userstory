import React from 'react';
import BaseWidget from './widget.jsx';
import {Line} from 'react-chartjs-2';
import './seventh_widget.scss';



export default class SeventhWidget extends BaseWidget {

  constructor(props) {
    super(props);
    this.state = {title: "init", list: ["init"]};
  }

  render() {
    let context = this;
    let Json_data = "";
    // console.log(this.props.Seventh_widget);
    if(this.props.Seventh_widget.data != undefined){
      let data = context.props.Seventh_widget.data
      data = data.replace(/ /g, '');
      data = data.replace('sprint','"sprint"')
      data = data.replace('dataset','"dataset"')
      Json_data = JSON.parse(data)
    }
    const data = {
      labels: Json_data.sprint,
      options: {
        legend: {
            labels: {
                fontColor: "black",
                fontSize: 12,
                fontFamily:'sans-serif'
            }
        },
        scales: {
            xAxes: [{
                ticks: {
                  fontColor: "black", // this here
                },
                scaleLabel: {
                  display: true,
                  labelString: 'number of sprints',
                  fontColor: "black",
                  fontSize: 12,
                  fontFamily:'sans-serif'
                }
            }],
            yAxes: [{
                display: true,
                ticks: {
                  fontColor: "black", // this here
                  fontFamily:'sans-serif'
                },
                scaleLabel: {
                  display: true,
                  labelString: 'velocity',
                  fontColor: "black",
                  fontSize: 12,
                  fontFamily:'sans-serif'
                }
            }],
        }
    },
      datasets: [
        {
          label: 'velocity',
          fill: false,
          lineTension: 0.1,
          backgroundColor: 'orange',
          borderColor: 'orange',
          pointBorderWidth: 1,
          data: Json_data.dataset
        },
      ],
    };
    // console.log(this.props,"??????????????????");

    return (
      <div  className={"seventh_widget widget w" + this.props.width + " h" + this.props.height}>
        <h2>{this.props.Seventh_widget.title}</h2>
        <div>
        <Line data={data} height={131} options={data.options}/>
      </div>
      </div>
    );
  }
}
