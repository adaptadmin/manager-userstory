import React from 'react';
import BaseWidget from './widget.jsx';
import {Line} from 'react-chartjs-2';
import './ninth_widget.scss';



export default class NinthWidget extends BaseWidget {

  constructor(props) {
    super(props);
    this.state = {title: "init", list: ["init"]};
  }

  render() {
    let context = this;
    let Json_data = "";
    // console.log(this.props.Ninth_widget);
    if(this.props.Ninth_widget.data != undefined){
      let data = context.props.Ninth_widget.data
      data = data.replace(/ /g, '');
      data = data.replace('sprint','"sprint"')
      data = data.replace('dataset','"dataset"')
      Json_data = JSON.parse(data)
    }
    const data = {
      labels: Json_data.sprint,
      options: {
        legend: {
            labels: {
                fontColor: "black",
                fontSize: 12,
                fontFamily:'sans-serif'

            }
        },
        scales: {
            xAxes: [{
                gridLines: {
                },
                ticks: {
                  fontColor: "black", // this here
                },
                scaleLabel: {
                  display: true,
                  labelString: 'number of days',
                  fontColor: "black",
                  fontSize: 12,
                  fontFamily:'sans-serif'
                }
            }],
            yAxes: [{
                display: true,
                gridLines: {

                },
                ticks: {
                  fontColor: "black", // this here
                  fontFamily:'sans-serif'
                },
                scaleLabel: {
                  display: true,
                  labelString: 'story points',
                  fontColor: "black",
                  fontSize: 12,
                  fontFamily:'sans-serif'
                }
            }],
        }
    },
      datasets: [
        {
          label: 'story points remaining',
          fill: false,
          lineTension: 0.1,
          backgroundColor: '#00A1CB',
          borderColor: '#00A1CB',
          pointBorderWidth: 1,
          data: Json_data.dataset
        },
      ]
    };
    // console.log(this.props,"??????????????????");

    return (
      <div  className={"ninth_widget widget w" + this.props.width + " h" + this.props.height}>
        <h2>{this.props.Ninth_widget.title}</h2>
        <div>
        <Line data={data} height={131} options={data.options}/>
      </div>
      </div>
    );
  }
}
