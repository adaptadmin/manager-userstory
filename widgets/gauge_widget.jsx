import React from 'react';
import BaseWidget from './widget.jsx';
import ReactSpeedometer from 'react-d3-speedometer';
import './gauge_widget.scss';

export default class GaugeWidget extends BaseWidget {

  constructor(props) {
    super(props);
    this.state = {title: "init", list: ["init"]};
  }

  render() {

    return (
      <div  className={"gauge_widget widget w" + this.props.width + " h" + this.props.height}>
        <h2>{this.props.Gauge_widget.title}</h2>
            {/* <h1 style={{fontSize:'8rem',textAlign:'center'}}>{this.props.Gauge_widget.data}</h1> */}
            <center style={{fontFamily:'sans-serif'}}>
              <ReactSpeedometer
                    maxValue={100}
                    value={parseInt(this.props.Gauge_widget.data)}
                    // needleColor="red"
                    startColor="red"
                    textColor = "black"
                    endColor="green"
                />
          </center>
      </div>
    );
  }
}
