import React from 'react';
import BaseWidget from './widget.jsx';

import './second_widget.scss';

export default class SecondWidget extends BaseWidget {

  constructor(props) {
    super(props);
    this.state = {title: "init", list: ["init"]};
  }

  render() {
    console.log(this.props.Second_widget.data);
    return (
      <div  className={"second_widget widget w" + this.props.width + " h" + this.props.height}>
        <h2>{this.props.Second_widget.title}</h2>
            <h1 style={{fontSize:'8rem',textAlign:'center'}}>{this.props.Second_widget.data}</h1>
      </div>
    );
  }
}
