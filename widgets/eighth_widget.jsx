import React from 'react';
import BaseWidget from './widget.jsx';

import './eighth_widget.scss';

export default class EighthWidget extends BaseWidget {

  constructor(props) {
    super(props);
    this.state = {title: "init", list: ["init"]};
  }

  render() {
    return (
      <div  className={"eighth_widget widget w" + this.props.width + " h" + this.props.height}>
        <h2>{this.props.Eighth_widget.title}</h2>
            <h1 style={{fontSize:'8rem',textAlign:'center'}}>{this.props.Eighth_widget.data}</h1>
      </div>
    );
  }
}
