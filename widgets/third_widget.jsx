import React from 'react';
import BaseWidget from './widget.jsx';

import './third_widget.scss';

export default class ThirdWidget extends BaseWidget {

  constructor(props) {
    super(props);
    this.state = {title: "init", list: ["init"]};
  }

  render() {
    return (
      <div  className={"third_widget widget w" + this.props.width + " h" + this.props.height}>
        <h2>{this.props.Third_widget.title}</h2>
            <h1 style={{fontSize:'8rem',textAlign:'center'}}>{this.props.Third_widget.data}</h1>
      </div>
    );
  }
}
