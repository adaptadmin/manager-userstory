import React from 'react';
import BaseWidget from './widget.jsx';
import {Line} from 'react-chartjs-2';
import './fifth_widget.scss';



export default class FifthWidget extends BaseWidget {

  constructor(props) {
    super(props);
    this.state = {title: "init", list: ["init"]};
  }

  render() {
    // console.log(this.props,'????????????????????');
    let context = this;
    let Json_data = "";
    if(this.props.Fifth_widget.data != undefined){
      let data = context.props.Fifth_widget.data;
      data = data.replace(/ /g, '');
      data = data.replace('sprint','"sprint"')
      data = data.replace('dataset1','"dataset1"')
      data = data.replace('dataset2','"dataset2"')
      Json_data = JSON.parse(data)

      // console.log(Json_data.dataHeads[0],".................",typeof Json_data.dataHeads);
    }
    const data = {
      labels: Json_data.sprint,
      options: {
        legend: {
            labels: {
                fontColor: "black",
                fontSize: 12,
                fontFamily:'sans-serif'
            }
        },
        scales: {
            xAxes: [{
                gridLines: {
                },
                ticks: {
                  fontColor: "black", // this here
                },
                scaleLabel: {
                  display: true,
                  labelString: 'number of sprints',
                  fontColor: "black",
                  fontSize: 12,
                  fontFamily:'sans-serif'
                }
            }],
            yAxes: [{
                display: true,
                gridLines: {

                },
                ticks: {
                  fontColor: "black", // this here
                  fontFamily:'sans-serif'
                },
                scaleLabel: {
                  display: true,
                  labelString: 'story points',
                  fontColor: "black",
                  fontSize: 12,
                  fontFamily:'sans-serif'
                }
            }],
        }
    },
      datasets: [
        {
          label: "planned",
          fill: false,
          lineTension: 0.1,
          backgroundColor: '#00A1CB',
          borderColor: '#00A1CB',
          pointBorderWidth: 1,
          data: Json_data.dataset1
        },
        {
          label: "completed",
          fill: false,
          lineTension: 0.1,
          backgroundColor: 'green',
          borderColor: 'green',
          pointBorderWidth: 1,
          data: Json_data.dataset2
        },
      ]
    };


    return (
      <div  className={"fifth_widget widget w" + this.props.width + " h" + this.props.height}>
        <h2>{this.props.Fifth_widget.title}</h2>
        <div>
        <Line data={data} height={131} options={data.options}/>
      </div>
      </div>
    );
  }
}
